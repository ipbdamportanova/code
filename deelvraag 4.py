import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

pd.set_option('display.width', 1000)
pd.set_option('display.max_rows', 1000)

# inladen data vooropleiding
FILE_PATH = '/Users/Sven/Documents/git/fackheelpythonenzijnmodules.xlsx'
vooropleiding = pd.read_excel(FILE_PATH)

file = "/Users/Sven/Documents/ipbdam3/cijfersbeideperiodes.xlsx"
cijfers = pd.read_excel(file)


#Wat is het verband tussen de aanwezigheid en de behaalde resultaten van de eerstejaars informatica studenten in de eerste periode? (per bulk, per klas)
cijfers['CijferIIBPM'] = ""
cijfers['CijferIARCH'] = ""

#behoud hoogste cijfer
cijfers["CijferIIBPM"] = cijfers[["CijferIIBPMt", "CijferIIBPMht"]].max(axis=1)
cijfers["CijferIARCH"] = cijfers[["CijferIARCHt", "CijferIARCHht"]].max(axis=1)

#merge files
vooropleiding = vooropleiding.merge(cijfers, left_on='Studentnummer', right_on='StudentNr', how='left')

vooropl = vooropleiding[['Studentnummer', 'Bulk', 'CijferIIBPMt', 'CijferIIBPMht','CijferIIBPM', 'CijferIARCHt', 'CijferIARCHht', 'CijferIARCH']]
print (vooropl.head(n=50))

vooropl.corr()
print(vooropl.corr())

testing = vooropleiding['CijferIIBPM'].where(vooropleiding['Bulk'] == 1 )

testing2 = testing.mean()
print ("Gemiddelde cijfer IIBPM bulk 1: {}".format(testing2))


testing = vooropleiding['CijferIIBPM'].where(vooropleiding['Bulk'] == 2 )
testing3 = testing.mean()
print ("Gemiddelde cijfer IIBPM bulk 2: {}".format(testing3))

testing = vooropleiding['CijferIARCH'].where(vooropleiding['Bulk'] == 1 )
testing4 = testing.mean()
print ("Gemiddelde cijfer IARCH bulk 1: {}".format(testing4))

testing = vooropleiding['CijferIARCH'].where(vooropleiding['Bulk'] == 2 )
testing5 = testing.mean()
print ("Gemiddelde cijfer IARCH bulk 2: {}".format(testing5))

groups = [[testing2, testing3],
      [testing4,testing5]]
group_labels = ["bulk1", "bulk2"]
num_items = len(group_labels)
ind = np.arange(num_items)
margin = 0.05
width = (1.-2.*margin)/num_items

s = plt.subplot(1,1,1)
for num, vals in enumerate(groups):
    print ("plotting: ", vals)
    # The position of the xdata must be calculated for each of the two data series
    xdata = ind+margin+(num*width)
    # Removing the "align=center" feature will left align graphs, which is what
    # this method of calculating positions assumes
    gene_rects = plt.bar(xdata, vals, width)
s.set_xticks(ind+0.273)
s.set_xticklabels(group_labels)
plt.title("IIBPM & IARCH")
plt.show()

