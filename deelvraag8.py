import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns

pd.set_option('display.width', 1000)
pd.set_option('display.max_rows', 1000)

# inladen data vooropleiding
FILE_PATH = '/Users/Sven/Documents/ipbdam3/SKC - anoniem.xlsx'
vooropleiding = pd.read_excel(FILE_PATH)

file = "/Users/Sven/Documents/ipbdam3/cijfersbeideperiodes.xlsx"
cijfers = pd.read_excel(file)


#Wat is het verband tussen de aanwezigheid en de behaalde resultaten van de eerstejaars informatica studenten in de eerste periode? (per bulk, per klas)
cijfers['CijferIIBPM'] = ""
cijfers['CijferIARCH'] = ""

#behoud hoogste cijfer
cijfers["CijferIIBPM"] = cijfers[["CijferIIBPMt", "CijferIIBPMht"]].max(axis=1)
cijfers["CijferIARCH"] = cijfers[["CijferIARCHt", "CijferIARCHht"]].max(axis=1)

#merge files
vooropleiding = vooropleiding.merge(cijfers, left_on='Studentnummer', right_on='StudentNr', how='left')

vooropl = vooropleiding[['Studentnummer', 'NOA Score', 'CijferIIBPMt', 'CijferIIBPMht','CijferIIBPM', 'CijferIARCHt', 'CijferIARCHht', 'CijferIARCH']]
print (vooropl.head(n=50))


aantallen = vooropleiding.groupby(['NOA Score']).size()

# plt.scatter
fig, ax = plt.subplots()

# Draw the graph
ax.plot(vooropleiding['CijferIARCH'], vooropleiding['NOA Score'], linestyle='', marker='o')

# Set the label for the x-axis
ax.set_xlabel("CijferIARCH")

# Set the label for the y-axis
ax.set_ylabel("NOA Score")

plt.show()